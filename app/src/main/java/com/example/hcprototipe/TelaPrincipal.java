package com.example.hcprototipe;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;

public class TelaPrincipal extends AppCompatActivity {

    Button BotaoConsulta;
    Button BotaoPerfil;
    Button BotaoSobre;
    Button BotaoSair;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_principal);

        BotaoConsulta = (Button)findViewById(R.id.BotaoConsulta);
        BotaoPerfil = (Button)findViewById(R.id.BotaoPerfil);
        BotaoSobre = (Button)findViewById(R.id.BotaoSobre);
        BotaoSair = (Button)findViewById(R.id.BotaoSair);

        verifyAuthentication();
    }


    private void verifyAuthentication(){

        if(FirebaseAuth.getInstance().getUid() == null){
            Intent it = new Intent(TelaPrincipal.this,TelaEntrar.class);
            it.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(it);
        }

    }

    public void clickConsulta(View view){
        Intent it = new Intent(this , TelaPreDig.class);
        startActivity(it);
    }

    public void clickPerfil(View view){
        Intent it = new Intent(this , TelaPerfil.class);
        startActivity(it);
    }

    public void clickSobre(View view){
        Intent it = new Intent(this , TelaSobre.class);
        startActivity(it);
    }

    public void clickSair(View view){

        FirebaseAuth.getInstance().signOut();
        verifyAuthentication();

    }
}
