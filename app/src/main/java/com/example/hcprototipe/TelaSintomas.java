package com.example.hcprototipe;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class TelaSintomas extends AppCompatActivity {

    Button BotaoEnviar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_sintomas);

        BotaoEnviar = (Button)findViewById(R.id.BotaoEnviar);
    }

    public void clickEnviar(View view){
        Intent it = new Intent(TelaSintomas.this,Contatos.class);
        startActivity(it);
        finish();
    }
}
