package com.example.hcprototipe;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;
import com.squareup.picasso.Picasso;
import com.xwray.groupie.GroupAdapter;

import java.util.List;

import javax.annotation.Nullable;

public class TelaPerfil extends AppCompatActivity {

    Usuario me = new Usuario();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_perfil);
        Profile();
    }

    private void Profile() {

        TextView txtNome = findViewById(R.id.txtRealNome);
        TextView txtEmail = findViewById(R.id.txtRealEmail);
        ImageView imgProfile = findViewById(R.id.profilepic);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        FirebaseFirestore.getInstance().collection("Users")
                .addSnapshotListener(new EventListener<QuerySnapshot>() {
                    @Override
                    public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                        if(e != null){
                            Log.e("Teste",e.getMessage(),e);
                            return;
                        }

                        List<DocumentSnapshot> docs  = queryDocumentSnapshots.getDocuments();


                        for(DocumentSnapshot doc : docs){
                            Usuario user = doc.toObject(Usuario.class);
                            Log.d("Teste",user.getUsername());
                            if(user.getUuid().equals(FirebaseAuth.getInstance().getUid())) {
                                me.setUsername(user.getUsername());
                                me.setProfileUrl(user.getProfileUrl());
                            }
                        }
                    }
                });

        if(user != null){

            txtNome.setText(me.getUsername());
            txtEmail.setText(user.getEmail());
            Picasso.get()
                    .load(me.getProfileUrl())
                    .into(imgProfile);
        }
        else{
            txtNome.setText("NULO");
            txtEmail.setText("NULO");
        }
    }
}
