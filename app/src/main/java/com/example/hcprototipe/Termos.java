package com.example.hcprototipe;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Termos extends AppCompatActivity {

    Button BotaoAceito;
    Button BotaoRecuso;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_termos);

        BotaoAceito = (Button)findViewById(R.id.BotaoAceito);
        BotaoRecuso = (Button)findViewById(R.id.BotaoRecuso);
    }

    public void clickAceito(View view){
        finish();
    }

    public void clickRecuso(View view){
        finishAffinity();
        finish();
        Intent it = new Intent(Termos.this , TelaEntrar.class);
        startActivity(it);
    }
}
