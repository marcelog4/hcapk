package com.example.hcprototipe;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class TelaEntrar extends AppCompatActivity {

    private EditText mEditEmail;
    private EditText mEditPassword;
    private Button mBtnLogin;
    private TextView mTxtCadastrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_entrar);

        mEditEmail = findViewById(R.id.EditEmail);
        mEditPassword = findViewById(R.id.EditPassword);
        mBtnLogin = findViewById(R.id.BtnEntrar);
        mTxtCadastrar = findViewById(R.id.TxtCadastrar);

        mBtnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String email = mEditEmail.getText().toString();
                String password = mEditPassword.getText().toString();

                Log.i("Teste" , email);
                Log.i("Teste", password);

                if(email == null || email.isEmpty() || password == null || password.isEmpty()){
                    Toast.makeText(TelaEntrar.this,"Nome , senha e email devem ser preenchidos!",Toast.LENGTH_SHORT).show();
                    return;

                }

                FirebaseAuth.getInstance().signInWithEmailAndPassword(email , password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(task.isSuccessful()){
                            Log.i("Teste",task.getResult().getUser().getUid());

                            Intent it = new Intent(TelaEntrar.this , TelaPrincipal.class);
                            startActivity(it);

                            finish();

                        }
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.i("Teste",e.getMessage());
                    }
                });

            }
        });

        mTxtCadastrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent it = new Intent(TelaEntrar.this , TelaCadastro.class);
                startActivity(it);
            }
        });
    }


}
