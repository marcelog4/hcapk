package com.example.hcprototipe;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class TelaPreDig extends AppCompatActivity {

    Button BotaoPresencial;
    Button BotaoDigital;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tela_pre_dig);

        BotaoPresencial = (Button)findViewById(R.id.BotaoPresencial);
        BotaoDigital = (Button)findViewById(R.id.BotaoDigital);
    }

    public void clickPresencial(View view){
        Intent it = new Intent(this , TelaPresencial.class);
        startActivity(it);
        finish();
    }

    public void clickDigital(View view){
        Intent it = new Intent(this , TelaSintomas.class);
        startActivity(it);
        finish();
    }

}
